# Code of Conduct

## Diversity Statement

> We value the participation of every member of our community and want to ensure that every contributor has a fun, enjoyable, and pleasant experience. Everyone who participates in the useR2021ZH Slack, the useR! Working group channel and meetings, the useR! Knowledgebase through GitLab/GitHub is expected to abide by this code of conduct and show respect and courtesy to other community members at all times. 


We–as members, contributors, and organisers–pledge to make participation in our community a harassment-free experience for everyone, regardless of their age, body size, visible or invisible disability, pregnancy, citizenship, ethnicity, sex characteristics, gender,  gender identity and expression, level of experience, education, socio-economic status, nationality, experience level, language, dialect, personal appearance, race, familial status, veteran status, religion, and sexual orientation.


**Complete [this Incident Report Form](https://docs.google.com/forms/d/e/1FAIpQLSfNRIyntK_6nOZ3Yd-DDCZ9DALVD-3l-D4rtWCSyma1bc-9tw/viewform) to alert the Code of Conduct Response Team if you notice someone in distress, or observe violations of this code of conduct, even if they seem inconsequential.**

## Expected Behaviour 

We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, supportive, and healthy community. In order to foster a positive and professional learning environment, we encourage the following kinds of behaviours in all meetings or participation in useR2021ZH Slack and GitLab/GitHub. 
Demonstrate empathy and kindness toward other people.

- **Use welcoming and inclusive language.**
- **Be respectful of differing opinions, viewpoints, and experiences.**
- **Give and gracefully accept constructive feedback.**
- **Accept responsibility** and apologise to those affected by our mistakes, and learn from the experience.
- **Act when you witness unacceptable behaviour.** If you see something inappropriate happening, speak up to the Code of Conduct Team. If you don't feel comfortable intervening but feel someone should, please feel free to ask a member of the Code of Conduct response team for support.
- **As an overriding general rule, please be intentional in your actions and humble in your mistakes.**
- **Try to understand why there are disagreements.** Disagreements, both social and technical, happen all the time. It is important that we resolve disagreements and differing views constructively. Remember that we’re different. Diversity contributes to the strength of our community, which is composed of people from a wide range of backgrounds.

## Unacceptable Behaviour

Behaviour that is unacceptable includes, but is not limited to:

- Offensive comments related to their age, body size, visible or invisible disability, pregnancy, citizenship, ethnicity, sex characteristics, gender, level of experience, education, socio-economic status, nationality, experience level, language, dialect, personal appearance, race, familial status, veteran status, religion, or sexual identity and orientation.
- Stalking, e.g. persistent unwanted direct messages, sending unwanted pictures or malware links, logging online activity for harassment purposes.
- Use of sexual or discriminatory imagery, comments, or jokes.
- Public or private harassment or intimidation.
- Sharing private conversations and discussions (e.g., screenshots of discussion channels or zoom, direct messages, email addresses, etc), without their explicit permission (except when reporting to the Code of Conduct Response Team)

> If you are asked to stop harassing behaviour, you should stop immediately, even if your behaviour was meant to be friendly or a joke. 

## Enforcement

Instances of abusive, harassing, or otherwise, unacceptable behaviour may be reported to the Code of Conduct Response Team by [completing the Code of Conduct Incident Report Form](https://docs.google.com/forms/d/e/1FAIpQLSfNRIyntK_6nOZ3Yd-DDCZ9DALVD-3l-D4rtWCSyma1bc-9tw/viewform). All complaints will be reviewed and investigated promptly and fairly and will result in a response appropriate to the circumstances.

All Code of Conduct Response Team members are obligated to respect the privacy and security of the reporter or those affected by the incident. They will respect confidentiality requests for the purpose of protecting the reporters. Correspondence will be handled confidentially and deleted after the case is resolved. All data will be stored securely, and access will be limited to the members of the Code Of Conduct Response Team who take the case. An anonymized transparency report will be drafted in June 2023, which will include a summary of the incident and the action taken without any details about dates or names. This transparency report might be kept permanently and made available to the community.


Individuals who are part of the Code of Conduct team agree to recuse themselves in the event of a conflict of interest associated with an incident report.  If you still have concerns about a conflict of interest, you are welcome to contact Rocío Joo  (rocio.joo@gmail.com) or Andrea Sánchez-Tapia (andreasancheztapia@gmail.com )  of the useR! Working Group.


## Code Of Conduct Response Team

- [Adithi R. Upadhya](https://adithirugis.netlify.app/)
- [Batool Almarzouq](https://batool-almarzouq.netlify.app/)
- [Paola Corrales](https://paocorrales.github.io/)

## Reporting Guidelines:  

If you believe someone violated the Code of Conduct, we ask that you report it. If you are not sure if the incident happened in the scope of our governed space, we ask that you still report the incident. You are encouraged to submit your report by [completing the Code of Conduct Incident Report Form](https://docs.google.com/forms/d/e/1FAIpQLSfNRIyntK_6nOZ3Yd-DDCZ9DALVD-3l-D4rtWCSyma1bc-9tw/viewform). 

[Submit a Code of Conduct Incident Report](https://docs.google.com/forms/d/e/1FAIpQLSfNRIyntK_6nOZ3Yd-DDCZ9DALVD-3l-D4rtWCSyma1bc-9tw/viewform).

All reports will be reviewed and investigated and will result in a response that is deemed necessary and appropriate to the circumstances. A report guarantees review, but not necessarily that an action will be taken.

## Attribution

This code of conduct was adapted from the [Contributor Covenant version 2.1](https://www.contributor-covenant.org/version/2/1/code_of_conduct.html), [The Carpentries](https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html), [The Turing Way](https://github.com/alan-turing-institute/the-turing-way/blob/main/CODE_OF_CONDUCT.md#:~:text=be%20kept%20confidential.-,2%20Code%20of%20Conduct,or%20causes%20discomfort%20to%20others.), [Open Life Science Program](https://openlifesci.org/code-of-conduct), [DiveRSE](https://diverse-rse.github.io/code_of_conduct), and [useR! 2021](https://user2021.r-project.org/participation/coc/) Code of Conduct.

This Code of Conduct is licensed under [a Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/), which means you can use, modify it and adapt it with attribution. It’s also adapted by other communities, including [ArabR](https://arabr.github.io/ArabR-Website/).

